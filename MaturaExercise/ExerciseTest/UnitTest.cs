﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaturaExercise;

namespace ExerciseTest
{

    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestLeitenderArztOhneVisitenToString()
        {
            LeitenderArzt l = new LeitenderArzt(1, "Niklas", "Moedlagl", Funktion.Primarius);

            Assert.AreEqual("Primarius: Moedlagl Niklas\n", l.ToString());
        }

        [TestMethod]
        public void TestLeitenderArztToString()
        {
            LeitenderArzt l = new LeitenderArzt(1, "Niklas", "Moedlagl", Funktion.Primarius);

            l.Visitieren(DateTime.Parse("10/10/2018"));

            Assert.AreEqual("Primarius: Moedlagl Niklas - Visiten: 10/10/2018\n", l.ToString());
        }

        [TestMethod]
        public void TestAssistenzarztOhneVisitenToString()
        {
            Assistenzarzt a = new Assistenzarzt(1, "Nico", "Windtner", false);

            Assert.AreEqual("Ass. Arzt: Windtner Nico [in Ausbildung]\n", a.ToString());
        }

        [TestMethod]
        public void TestAssistenzarztToString()
        {
            Assistenzarzt a = new Assistenzarzt(1, "Nico", "Windtner", true);

            a.Visitieren(DateTime.Parse("01/10/2018"));
            a.Visitieren(DateTime.Parse("02/20/2018"));

            Assert.AreEqual("Ass. Arzt: Windtner Nico - Visiten: 1/10/2018 2/20/2018 [fertig]\n", a.ToString());
        }

        [TestMethod]
        public void TestTurnusarztOhneVisitenToString()
        {
            Turnusarzt t = new Turnusarzt(1, "Thomas", "Himmer", 2);

            Assert.AreEqual("Turnusarzt: Himmer Thomas [2 Monate]\n", t.ToString());
        }

        [TestMethod]
        public void TestTurnusarztMitEinemMonatToString()
        {
            Turnusarzt t = new Turnusarzt(1, "Thomas", "Himmer", 1);

            t.Visitieren(DateTime.Parse("3/20/2018"));

            Assert.AreEqual("Turnusarzt: Himmer Thomas - Visiten: 3/20/2018 [1 Monat]\n", t.ToString());
        }

        [TestMethod]
        public void TestTurnusarztToString()
        {
            Turnusarzt t = new Turnusarzt(1, "Thomas", "Himmer", 2);

            t.Visitieren(DateTime.Parse("3/20/2018"));

            Assert.AreEqual("Turnusarzt: Himmer Thomas - Visiten: 3/20/2018 [2 Monate]\n", t.ToString());
        }

        [TestMethod]
        public void TestArztErstellen()
        {
            LeitenderArzt arzt = new LeitenderArzt(1,"Nico", "Windtner", Funktion.Oberarzt);

            Assert.IsNotNull(arzt.visiten);
        }

        [TestMethod]
        public void TestAbteilungen()
        {
            Abteilung abt = new Abteilung(1, "IT");

            Assistenzarzt a = new Assistenzarzt(1, "Nico", "Windtner", true);

            a.Visitieren(DateTime.Parse("01/10/2018"));
            a.Visitieren(DateTime.Parse("02/20/2018"));

            abt.arztHinzufuegen(a);

            Assert.AreEqual("Abteilung: IT\n\nÄrzteteam:\n----------\nAss. Arzt: Windtner Nico - Visiten: 1/10/2018 2/20/2018 [fertig]\n", abt.ToString());
        }

        [TestMethod]
        public void TestSortierenNachArzttyp()
        {
            Abteilung abt = new Abteilung(1, "IT");

            Assistenzarzt a = new Assistenzarzt(1, "Nico", "Windtner", true);
            LeitenderArzt l = new LeitenderArzt(2, "Niklas", "Moedlagl", Funktion.Primarius);
            Turnusarzt t = new Turnusarzt(3, "Thomas", "Himmer", 1);

            abt.arztHinzufuegen(a);
            abt.arztHinzufuegen(l);
            abt.arztHinzufuegen(t);

            Assert.AreEqual("Abteilung: IT\n\nÄrzteteam:\n----------\nPrimarius: Moedlagl Niklas\nAss. Arzt: Windtner Nico [fertig]\nTurnusarzt: Himmer Thomas [1 Monat]\n", abt.ToString());
        }

        [TestMethod]
        public void TestSortierenNachFunktion()
        {
            Abteilung abt = new Abteilung(1, "IT");

            LeitenderArzt l1 = new LeitenderArzt(1, "Niklas", "Moedlagl", Funktion.Primarius);
            LeitenderArzt l2 = new LeitenderArzt(2, "Nico", "Windtner", Funktion.Oberarzt);
            LeitenderArzt l3 = new LeitenderArzt(3, "Thomas", "Himmer", Funktion.Oberarzt_Stv);

            abt.arztHinzufuegen(l3);
            abt.arztHinzufuegen(l2);
            abt.arztHinzufuegen(l1);

            Assert.AreEqual("Abteilung: IT\n\nÄrzteteam:\n----------\nPrimarius: Moedlagl Niklas\nOberarzt: Windtner Nico\nOberarzt_Stv: Himmer Thomas\n", abt.ToString());
        }



    }
}
