﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaturaExercise
{
    public class Assistenzarzt : Arzt, IAktivitaeten
    {

        public bool AusbildungAbgeschlossen { get; set; }

        public Assistenzarzt(int id, string v, string n, bool aus) : base(id, v, n)
        {
            this.AusbildungAbgeschlossen = aus;
        }

        public void operieren()
        {
            throw new NotImplementedException();
        }

        public void PatientenAufnehmen()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            string visiten = "";
            if (this.visiten.Count != 0)
            {
                visiten += " - Visiten:";
                foreach (DateTime item in this.visiten)
                {
                    visiten += " " + item.ToShortDateString();
                }
            }

            string status;
            if (this.AusbildungAbgeschlossen)
                status = " [fertig]";
            else
                status = " [in Ausbildung]";

            return "Ass. Arzt: " + this.Nachname + " " + this.Vorname + visiten + status + "\n";
        }

    }
}
