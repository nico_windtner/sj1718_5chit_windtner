﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaturaExercise
{
    public abstract class Arzt
    {

        public int ID { get; set; }

        public string Vorname { get; set; }

        public string Nachname { get; set; }

        public List<DateTime> visiten;

        public Arzt(int id, string v, string n)
        {
            this.ID = id;
            this.Vorname = v;
            this.Nachname = n;
            this.visiten = new List<DateTime>();
        }

        public void Visitieren(DateTime d)
        {
            visiten.Add(d);
        }

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
