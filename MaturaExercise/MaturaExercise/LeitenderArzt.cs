﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaturaExercise
{
    public class LeitenderArzt : Arzt, IAktivitaeten
    {
        public Funktion Funkt { get; set; }

        public LeitenderArzt(int id, string v, string n, Funktion f) : base(id, v, n)
        {
            this.Funkt = f;
        }

        public void operieren()
        {

        }

        public void PatientenAufnehmen()
        {

        }

        public override string ToString()
        {
            string visiten = "";
            if (this.visiten.Count != 0)
            {
                visiten += " - Visiten:";
                foreach (DateTime item in this.visiten)
                {
                    visiten += " " + item.ToShortDateString();
                }
            }

            return this.Funkt.ToString() + ": " + this.Nachname + " " + this.Vorname + visiten + "\n";
        }

    }
}
