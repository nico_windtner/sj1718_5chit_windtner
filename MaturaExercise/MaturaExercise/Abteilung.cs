﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaturaExercise
{
    public class Abteilung
    {

        private int ID { get; set; }

        private string Bezeichnung { get; set; }

        private List<Arzt> arztListe;

        public Abteilung(int id, string b)
        {
            this.ID = id;
            this.Bezeichnung = b;
            this.arztListe = new List<Arzt>();
        }

        public void arztHinzufuegen(Arzt a)
        {
            arztListe.Add(a);
        }

        public override string ToString()
        {
            List<LeitenderArzt> leitendeAerzte = new List<LeitenderArzt>();
            List<Assistenzarzt> assitenzAerzte = new List<Assistenzarzt>();
            List<Turnusarzt> turnAerzte = new List<Turnusarzt>();

            foreach (Arzt item in arztListe)
            {
                if (item is LeitenderArzt)
                    leitendeAerzte.Add((LeitenderArzt)item);
                else if (item is Assistenzarzt)
                    assitenzAerzte.Add((Assistenzarzt)item);
                else
                    turnAerzte.Add((Turnusarzt)item);
            }

            leitendeAerzte.Sort((a, b) => a.Funkt.CompareTo(b.Funkt));
            string alleAerzte = "";
            foreach (LeitenderArzt item in leitendeAerzte)
                alleAerzte += item.ToString();
            foreach (Assistenzarzt item in assitenzAerzte)
                alleAerzte += item.ToString();
            foreach (Turnusarzt item in turnAerzte)
                alleAerzte += item.ToString();

            return "Abteilung: " + this.Bezeichnung + "\n\nÄrzteteam:\n----------\n" + alleAerzte;
        }

    }
}
