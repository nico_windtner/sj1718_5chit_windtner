﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaturaExercise
{
    public class Turnusarzt : Arzt
    {

        public int Dauer { get; set; }

        public Turnusarzt(int id, string v, string n, int dauer) : base(id, v, n)
        {
            this.Dauer = dauer;
        }

        public override string ToString()
        {
            string visiten = "";
            if (this.visiten.Count != 0)
            {
                visiten += " - Visiten:";
                foreach (DateTime item in this.visiten)
                {
                    visiten += " " + item.ToShortDateString();
                }
            }

            return "Turnusarzt: " + this.Nachname + " " + this.Vorname + visiten + " [" + this.Dauer + " " + (this.Dauer == 1 ? "Monat" : "Monate") + "]" + "\n";
        }

    }
}
