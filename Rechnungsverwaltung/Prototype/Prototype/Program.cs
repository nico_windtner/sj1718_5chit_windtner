﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPdf;
using FileOperations;


namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            HtmlToPdf renderer = new HtmlToPdf();
            renderer.PrintOptions.PrintHtmlBackgrounds = false;
            renderer.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;
            renderer.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            renderer.PrintOptions.FirstPageNumber = 1;

            HtmlHeaderFooter header = new HtmlHeaderFooter();
            header.HtmlFragment = "<h3>Header</h3>";

            HtmlHeaderFooter footer = new HtmlHeaderFooter();
            footer.HtmlFragment = "<p>Seite {page} von {total-pages}</p>";

            renderer.PrintOptions.Header = header;
            renderer.PrintOptions.Footer = footer;

            CreateTablePDF(renderer);
        }

        private static void CreateTablePDF(HtmlToPdf renderer)
        {
            FileLoader file = new FileLoader("table.html");
            string content = file.Load();
            renderer.RenderHtmlAsPdf(content).SaveAs("table.pdf");
        }

    }
}
