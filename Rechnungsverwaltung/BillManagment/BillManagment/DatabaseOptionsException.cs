﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment
{
    public class DatabaseOptionsException : Exception
    {

        public DatabaseOptionsException() : base() { }

        public DatabaseOptionsException(string message) : base(message) { }

        public DatabaseOptionsException(string message, Exception inner) : base(message, inner) { }

    }
}
