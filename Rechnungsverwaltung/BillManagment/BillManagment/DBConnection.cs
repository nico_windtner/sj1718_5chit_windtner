﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace BillManagment
{
    class DBConnection
    {

        private static DBConnection _instance = null;

        private MySqlConnection Connection;

        public String Database { get; set; } = null;

        public String Username { get; set; } = null;

        public String Password { get; set; } = null;

        private DBConnection()
        {

        }

        public static DBConnection GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DBConnection();
                return _instance;
            }
            else
                return _instance;
        }

        public void Open()
        {
            if (Connection != null)
                Connection.Close();

            if (Database == null)
                throw new DatabaseOptionsException("The field database is not set.");

            if (Username == null)
                throw new DatabaseOptionsException("The field username is not set.");

            if (Password == null)
                throw new DatabaseOptionsException("The field password is not set.");

            String connString = $"Server=localhost; database={Database}; UID={Username}; password={Password}";
            Connection = new MySqlConnection(connString);
            Connection.Open();
        }

        public MySqlCommand CreateCommand() => Connection.CreateCommand();

        public void Close()
        {
            if (Connection != null)
            {
                Connection.Close();
            }
        }

    }
}
