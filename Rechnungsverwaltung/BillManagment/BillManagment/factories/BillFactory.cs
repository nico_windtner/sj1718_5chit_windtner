﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillManagment.model;
using MySql.Data.MySqlClient;

namespace BillManagment.factories
{
    class BillFactory
    {

        public static Bill GetBill(int id)
        {
            Contractor contractor = CustomerFactory.GetContractor(id);
            Customer customer = CustomerFactory.GetCustomer(id);
            List<Article> articles = ArticleFactory.GetArticles(id);

            DBConnection connection = DBConnection.GetInstance();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "" +
                "SELECT ORDER_ID AS ORDER_NUMBER, DATE, DISCOUNT " +
                "FROM ORDERS " +
                "WHERE ORDER_ID = 1";
            command.Parameters.AddWithValue("@orderId", id);

            MySqlDataReader dataReader = command.ExecuteReader();
            dataReader.Read();

            Bill bill = new Bill();
            bill.Number = dataReader.GetString(0);
            bill.Date = dataReader.GetDateTime(1);
            bill.Discount = dataReader.GetDouble(2);
            bill.Customer = customer;
            bill.Contractor = contractor;
            bill.Articles = articles;

            return bill;
        }

    }
}
