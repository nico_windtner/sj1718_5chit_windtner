﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using BillManagment.model;

namespace BillManagment.factories
{
    class ArticleFactory
    {

        public static List<Article> GetArticles(int orderId)
        {
            DBConnection connection = DBConnection.GetInstance();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "" +
                "SELECT A.TITLE, A.PRICE, A.UNIT, D.PERCENTAGE AS DISCOUNT, OA.AMOUNT " +
                "FROM ARTICLES A " +
                "JOIN ORDERS_ARTICLES OA ON A.ARTICLE_ID = OA.ARTICLE_ID " +
                "LEFT JOIN DISCOUNTS D ON A.ARTICLE_ID = D.ARTICLE_ID " +
                "AND CURRENT_DATE() < D.EXPIRE_DATE " +
                "WHERE OA.ORDER_ID = @orderId";
            command.Parameters.AddWithValue("@orderId", orderId);

            List<Article> articles = new List<Article>();

            MySqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Article article = new Article();
                article.Title = dataReader.GetString(0);
                article.Price = dataReader.GetDouble(1);
                article.Unit = dataReader.GetString(2);
                article.Discount = dataReader.IsDBNull(3) ? 0 : dataReader.GetDouble(3);
                article.Amount = dataReader.GetDouble(4);

                articles.Add(article);
            }

            return articles;
        }

    }
}
