﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using BillManagment.model;

namespace BillManagment.factories
{
    class CustomerFactory
    {

        public static Contractor GetContractor(int orderId)
        {
            DBConnection connection = DBConnection.GetInstance();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "" +
                "SELECT C.NAME AS COMPANY_NAME, C.STREET_NAME, C.STREET_NUMBER, CI.ZIP, CI.NAME AS CITY_NAME, CO.NAME AS COUNTRY_NAME, C.PHONE, C.MAIL, C.INTERNET, C.LOGO_PATH, C.ADVERTISMENT, BI.NAME AS BANK_NAME, BI.BIC, BI.IBAN, C.VAT " +
                "FROM COMPANIES C " +
                "JOIN BANK_INFORMATIONS BI ON C.BANK_INFORMATION_ID = BI.BANK_INFORMATION_ID " +
                "JOIN CITIES CI ON C.CITY_ID = CI.CITY_ID " +
                "JOIN COUNTRIES CO ON CI.COUNTRY_ID = CO.COUNTRY_ID " +
                "JOIN ORDERS O ON C.COMPANY_ID = O.CONTRACTOR_ID " +
                "WHERE O.ORDER_ID = @orderId";
            command.Parameters.AddWithValue("@orderId", orderId);

            MySqlDataReader dataReader = command.ExecuteReader();
            dataReader.Read();

            Contractor contractor = new Contractor();
            contractor.Name = dataReader.GetString(0);
            contractor.StreetName = dataReader.GetString(1);
            contractor.StreetNumber = dataReader.GetString(2);
            contractor.Location = new Location();
            contractor.Location.ZIPCode = dataReader.GetString(3);
            contractor.Location.City = dataReader.GetString(4);
            contractor.Location.Country = dataReader.GetString(5);
            contractor.Phone = dataReader.GetString(6);
            contractor.EMail = dataReader.GetString(7);
            contractor.Internet = dataReader.GetString(8);
            contractor.ImgPath = dataReader.GetString(9);
            contractor.Advertisment = dataReader.GetString(10);
            contractor.BankInformation = new BankInformation();
            contractor.BankInformation.Name = dataReader.GetString(11);
            contractor.BankInformation.BIC = dataReader.GetString(12);
            contractor.BankInformation.IBAN = dataReader.GetString(13);
            contractor.VAT = dataReader.GetString(14);

            return contractor;
        }

        public static Customer GetCustomer(int orderId)
        {
            DBConnection connection = DBConnection.GetInstance();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "" +
                "SELECT C.NAME AS COMPANY_NAME, C.STREET_NAME, C.STREET_NUMBER, CI.ZIP, CI.NAME AS CITY_NAME, CO.NAME AS COUNTRY_NAME, C.PHONE, C.MAIL, C.INTERNET, C.OWNER_NAME, C.COMPANY_ID " +
                "FROM COMPANIES C " +
                "JOIN BANK_INFORMATIONS BI ON C.BANK_INFORMATION_ID = BI.BANK_INFORMATION_ID " +
                "JOIN CITIES CI ON C.CITY_ID = CI.CITY_ID " +
                "JOIN COUNTRIES CO ON CI.COUNTRY_ID = CO.COUNTRY_ID " +
                "JOIN ORDERS O ON C.COMPANY_ID = O.CUSTOMER_ID " +
                "WHERE O.ORDER_ID = @orderId";
            command.Parameters.AddWithValue("@orderId", orderId);

            MySqlDataReader dataReader = command.ExecuteReader();
            dataReader.Read();

            Customer customer = new Customer();
            customer.Name = dataReader.GetString(0);
            customer.StreetName = dataReader.GetString(1);
            customer.StreetNumber = dataReader.GetString(2);
            customer.Location = new Location();
            customer.Location.ZIPCode = dataReader.GetString(3);
            customer.Location.City = dataReader.GetString(4);
            customer.Location.Country = dataReader.GetString(5);
            customer.Phone = dataReader.GetString(6);
            customer.EMail = dataReader.GetString(7);
            customer.Internet = dataReader.GetString(8);
            customer.OwnerName = dataReader.GetString(9);
            customer.Number = dataReader.GetInt32(10);

            return customer;
        }

    }
}
