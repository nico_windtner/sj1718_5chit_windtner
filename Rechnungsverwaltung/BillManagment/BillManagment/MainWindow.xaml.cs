﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IronPdf;
using BillManagment.factories;
using BillManagment.model;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace BillManagment
{
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            DBConnection connection = DBConnection.GetInstance();
            connection.Database = "school";
            connection.Username = "root";
            connection.Password = "";

            this.InitializeComponent();

            List<int> numbers = this.GetBillNumbers();
            foreach (int item in numbers)
                lbBillNumbers.Items.Add(item);
        }

        public List<int> GetBillNumbers()
        {
            List<int> billNumbers = new List<int>();

            DBConnection connection = DBConnection.GetInstance();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "" +
                "SELECT ORDER_ID " +
                "FROM ORDERS " +
                "ORDER BY ORDER_ID ASC";

            MySqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
                billNumbers.Add(dataReader.GetInt32(0));

            return billNumbers;
        }

        private void lbBillNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnCreate.IsEnabled = true;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            int number = Convert.ToInt32(lbBillNumbers.Items[lbBillNumbers.SelectedIndex]);

            Bill bill = BillFactory.GetBill(number);

            HtmlToPdf renderer = new HtmlToPdf();
            renderer.PrintOptions.PrintHtmlBackgrounds = false;
            renderer.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;
            renderer.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            renderer.PrintOptions.FirstPageNumber = 1;

            HtmlHeaderFooter header = new HtmlHeaderFooter();
            header.HtmlFragment = "" +
                "<div>" +
                "<div class=\"header\">" +
                "   <style>" +
                "       .header {" +
                "           font-size: 16px;" +
                "           font-family: Arial, Helvetica, sans-serif;" +
                "           height: 80px;" +
                "           margin-bottom: 30px;" +
                "       }" +
                "       .header img {" +
                "           float: left;" +
                "           height: 60px;" +
                "           margin-right: 10px;" +
                "       }" +
                "       h2 {" +
                "           float: left;" +
                "           line-height: 60px;" +
                "           margin: 0;" +
                "       }" +
                "       .number {" +
                "           float: right;" +
                "           line-height: 40px;" +
                "           text-align: right;" +
                "       }" +
                "       .right { float: right; width: 300px; }" +
                "       p { text-align: right; margin: 0; line-height: 20px; }" +
                "   </style>" +
                "   <img src=\"" + bill.Contractor.ImgPath + "\"/>" +
                "   <h2>" + bill.Contractor.Name + "</h2>" +
                "<div class=\"right\">" +
                "   <h2 class=\"number\">Rechnungsnummer: " + bill.Number + "</h2>" +
                "   <p><b>Kundenname:</b> " + bill.Customer.Name + "</p>" +
                "   <p><b>Kundennummer:</b> " + bill.Customer.Number + "</p>" +
                "</div>";
            header.Height = 40;

            HtmlHeaderFooter headerFirst = new HtmlHeaderFooter();
            headerFirst.HtmlFragment = "" +
                "<div class=\"header\">" +
                "   <style>" +
                "       .header {" +
                "           font-size: 16px;" +
                "           font-family: Arial, Helvetica, sans-serif;" +
                "           height: 60px;" +
                "       }" +
                "       .header img {" +
                "           float: left;" +
                "           height: 60px;" +
                "           margin-right: 10px;" +
                "       }" +
                "       h2 {" +
                "           float: left;" +
                "           line-height: 60px;" +
                "           margin: 0;" +
                "       }" +
                "       .number {" +
                "           float: right;" +
                "           line-height: 60px;" +
                "       }" +
                "   </style>" +
                "   <img src=\"" + bill.Contractor.ImgPath + "\"/>" +
                "   <h2>" + bill.Contractor.Name + "</h2>" +
                "   <h2 class=\"number\">Rechnungsnummer: " + bill.Number + "</h2>" +
                "</div>";
            headerFirst.Height = 40;

            HtmlHeaderFooter footer = new HtmlHeaderFooter();
            footer.HtmlFragment = "" +
                "<div class=\"footer\">" +
                "   <style>" +
                "       .footer {" +
                "           font-size: 16px;" +
                "           font-family: Arial, Helvetica, sans-serif;" +
                "           height: 48px;" +
                "           padding-top: 2px;" +
                "       }" +
                "       .left {" +
                "           width: 50%;" +
                "           float: left;" +
                "       }" +
                "       .left p {" +
                "           line-height: 12px;" +
                "           text-align: left;" +
                "       }" +
                "       .right {" +
                "           width: 50%;" +
                "           float: right;" +
                "       }" +
                "       .right p {" +
                "           line-height: 12px;" +
                "           text-align: right;" +
                "       }" +
                "   </style>" +
                "   <p style=\"text-align: center;\"><i>" + bill.Contractor.Advertisment + "</i></p>" +
                "   <div class=\"left\">" +
                "       <p><i>Überweisung bitte an:</i></p>" +
                "       <p><b>" + bill.Contractor.BankInformation.Name + "</b></p>" +
                "       <p><b>IBAN:</b> " + bill.Contractor.BankInformation.IBAN + "</p>" +
                "       <p><b>BIC:</b> " + bill.Contractor.BankInformation.BIC + "</p>" +
                "   </div>" +
                "   <div class=\"right\">" +
                "       <p><b>UID:</b> " + bill.Contractor.VAT + "</p>" +
                "       <p>Seite {page} von {total-pages}</p>" +
                "   </div>" +
                "</div>";
            footer.Height = 65;
            footer.DrawDividerLine = true;

            HtmlHeaderFooter placeHolderHeader = new HtmlHeaderFooter();
            placeHolderHeader.Height = 50;

            renderer.PrintOptions.Header = placeHolderHeader;
            renderer.PrintOptions.Footer = footer;
            PdfDocument document = renderer.RenderHtmlAsPdf(bill.ToString());
            document.AddHTMLHeaders(header, true);
            document.AddHTMLHeaders(headerFirst, false, new[] { 0 });

            document.SaveAs("output.pdf");
            Process.Start("output.pdf");

            this.Close();
        }
    }
}
