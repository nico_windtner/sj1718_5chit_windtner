﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    class BankInformation
    {

        public String Name { get; set; }

        public String BIC { get; set; }

        public String IBAN { get; set; }

    }
}
