﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    class Bill
    {

        public String Number { get; set; }

        public DateTime Date { get; set; }

        public Double Discount { get; set; }

        public Customer Customer { get; set; }

        public Contractor Contractor { get; set; }

        public List<Article> Articles { get; set; }

        private double GetSum()
        {
            double sum = 0;

            foreach (Article item in Articles)
                sum += (item.Price - (item.Price * (item.Discount / 100)));

            return sum;
        }

        public override string ToString()
        {
            string articles = "";

            foreach (Article item in Articles)
                articles += item.ToString();

            return "" +
                "<html>" +
                "   <head>" +
                "       <style>" +
                "           body {" +
                "               font-size: 16px;" +
                "               font-family: Arial, Helvetica, sans-serif;" +
                "               margin-top: 30px;" +
                "           }" +
                "           .text-left {" +
                "               text-align: left;" +
                "           }" +
                "           .text-center {" +
                "               text-align: center;" +
                "           }" +
                "           .text-right {" +
                "               text-align: right;" +
                "           }" +
                "           .text-top {" +
                "               vertical-align: top;" +
                "           }" +
                "           .text-bottom {" +
                "               vertical-align: bottom;" +
                "           }" +
                "           .float-left {" +
                "               float: left;" +
                "           }" +
                "           .float-right {" +
                "               float: right;" +
                "           }" +
                "           .clear-both {" +
                "               clear: both;" +
                "           }" +
                "           table {" +
                "               margin-top: 70px;" +
                "               width: 100%;" +
                "               border-collapse: collapse;" +
                "           }" +
                "           th {" +
                "               padding: 12px 5px;" +
                "               border-top: 1px solid #000;" +
                "               border-bottom: 1px solid #000;" +
                "           }" +
                "           tr > td {" +
                "               padding: 20px 5px;" +
                "               min-width: 60px;" +
                "           }" +
                "           .header { " +
                "               font-size: 16px;" +
                "               font-family: Arial, Helvetica, sans-serif;" +
                "               height: 60px;" +
                "               margin-bottom: 50px;" +
                "           }" +
                "           .header img {" +
                "               float: left;" +
                "               height: 60px;" +
                "               margin-right: 10px;" +
                "           }" +
                "           h2 {" +
                "               float: left;" +
                "               line-height: 60px;" +
                "               margin: 0;" +
                "           }" +
                "           .number {" +
                "               float: right;" +
                "               line-height: 60px;" +
                "           }" +
                "           .td-foot {" +
                "               padding: 10px 5px;" +
                "           }" +
                "           .line-above {" +
                "               padding-top: 20px;" +
                "               border-top: 1px solid #000;" +
                "           }" +
                "       </style>" +
                "   </head>" +
                "   <body>" +
                this.Contractor.ToString() +
                "       <p class=\"text-right\">" +
                "           <b>Datum:</b> " + this.Date.ToShortDateString() + "<br>" +
                "       </p>" +
                this.Customer.ToString() +
                "       <table>" +
                "           <thead>" +
                "               <tr>" +
                "                   <th class=\"text-left\">Artikelbezeichnung</th>" +
                "                   <th class=\"text-right\">Menge</th>" +
                "                   <th class=\"text-right\">Preis</th>" +
                "                   <th class=\"text-right\">Rabatt</th>" +
                "                   <th class=\"text-right\">BETRAG</th>" +
                "               </tr>" +
                "           <thead>" +
                "           <tbody>" +
                articles +
                "               <tr>" +
                "                   <td class=\"line-above td-foot\"/><td class=\"line-above td-foot\"/><td class=\"line-above td-foot\"/>" +
                "                   <td class=\"line-above td-foot text-right\"><b>Gesamtsumme (Netto)</b></td>" +
                "                   <td class=\"line-above td-foot text-right\">" + Math.Round(this.GetSum(), 2) + "€</td>" +
                "               </tr>" +
                "               <tr>" +
                "                   <td class=\"td-foot\"/><td class=\"td-foot\"/><td class=\"td-foot\"/>" +
                "                   <td class=\"td-foot text-right\"><b>Abzüglich Rabatt (" + this.Discount + "%)</b></td>" +
                "                   <td class=\"td-foot text-right\">" + Math.Round(this.GetSum() - (this.GetSum() * (this.Discount / 100)), 2) + "€</td>" +
                "               </tr>" +
                "               <tr>" +
                "                   <td class=\"td-foot\"/><td class=\"td-foot\"/><td class=\"td-foot\"/>" +
                "                   <td class=\"td-foot text-right\"><b>Ust 20%</b></td>" +
                "                   <td class=\"td-foot text-right\">" + Math.Round((this.GetSum() - (this.GetSum() * (this.Discount / 100))) * 0.2, 2) + "€</td>" +
                "               </tr>" +
                "               <tr>" +
                "                   <td class=\"td-foot\"/><td class=\"td-foot\"/><td class=\"td-foot\"/>" +
                "                   <td class=\"td-foot text-right\"><b>Gesamtsumme (Brutto)</b></td>" +
                "                   <td class=\"td-foot text-right\">" + Math.Round((this.GetSum() - (this.GetSum() * (this.Discount / 100))) * 1.2, 2) + "€</td>" +
                "               </tr>" +
                "           </tbody>" +
                "       </table>" +
                "   </body>" +
                "</html>";
        }

    }
}
