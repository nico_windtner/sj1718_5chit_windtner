﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    class Location
    {

        public String ZIPCode { get; set; }

        public String City { get; set; }

        public String Country { get; set; }

    }
}
