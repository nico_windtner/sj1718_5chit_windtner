﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    abstract class ACompany
    {

        public String Name { get; set; }

        public String StreetName { get; set; }

        public String StreetNumber { get; set; }

        public Location Location { get; set; }

        public String Phone { get; set; }

        public String EMail { get; set; }

        public String Internet { get; set; }

    }
}
