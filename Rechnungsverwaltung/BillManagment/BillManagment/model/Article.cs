﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    class Article
    {

        public String Title { get; set; }

        public Double Price { get; set; }

        public String Unit { get; set; }

        public Double Discount { get; set; }

        public Double Amount { get; set; }

        public override string ToString()
        {
            string discount = ((this.Price * (this.Discount / 100)) == 0) ? "0€" : Math.Round((this.Price * (this.Discount / 100)), 2).ToString() + "€ (" + this.Discount + "%)";

            return "" +
                "<tr>" +
                "<td class=\"text-left text-top\">" + this.Title + "</td>" +
                "<td class=\"text-right text-top\">" + this.Amount + " " + this.Unit + "</td>" +
                "<td class=\"text-right text-top\">" + this.Price + "€</td>" +
                "<td class=\"text-right text-top\">" + discount + "</td>" + 
                "<td class=\"text-right text-top\">" + Math.Round(this.Price - (this.Price * (this.Discount / 100)), 2)  + "€</td>" +
                "</tr>";
        }

    }
}
