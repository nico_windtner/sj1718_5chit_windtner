﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    class Contractor : ACompany
    {

        public String ImgPath { get; set; }

        public String Advertisment { get; set; }

        public BankInformation BankInformation { get; set; }

        public String VAT { get; set; }

        public override string ToString()
        {
            return "" +
                "<p>" +
                this.StreetName + " " + this.StreetNumber + "<br>" +
                this.Location.ZIPCode + " " + this.Location.City + "<br>" +
                this.Location.Country + "<br>" +
                "Telefon: " + this.Phone + "<br>" +
                "E-Mail: " + this.EMail + "<br>" +
                "Internet: " + this.Internet + "<br>" +
                "</p>";
        }

    }
}
