﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillManagment.model
{
    class Customer : ACompany
    {

        public String OwnerName { get; set; }

        public int Number { get; set; }

        public override string ToString()
        {
            return "" +
                "<p>" +
                "<b>Empfänger:</b><br>" +
                this.OwnerName + "<br>" +
                this.Name + "<br>" +
                this.StreetName + " " + this.StreetNumber + "<br>" +
                this.Location.ZIPCode + " " + this.Location.City + "<br>" +
                this.Location.Country + "<br>" +
                "Telefon: " + this.Phone + "<br>" +
                "E-Mail: " + this.EMail + "<br>" +
                "Internet: " + this.Internet + "<br>" +
                "Kundennummer: " + this.Number +
                "</p>";
        }

    }
}
