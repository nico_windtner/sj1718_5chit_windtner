﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileOperations
{
    public class FileLoader
    {

        private string filepath;

        public FileLoader(string filepath)
        {
            this.filepath = filepath;
        }

        public string Load()
        {
            if (!File.Exists(filepath))
                return "";

            StreamReader reader = new StreamReader(filepath);
            string content = "";

            while (reader.Peek() != -1)
                content += reader.ReadLine();

            return content;
        }

    }
}
