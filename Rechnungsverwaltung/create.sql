USE SCHOOL;


INSERT INTO ARTICLES (TITLE, PRICE, UNIT)
	VALUES ('APPLE MacBook Air 13.3" 128 GB (MQD32D/A)', 999, ''),
    ('LOGITECH PC Maus MX Master 2S, kabellos, schwarz (910-005139)', 89.99, ''),
    ('ASUS Notebook R752NA-TY028T, schwarz (90NB0EA1-M00420)', 449, ''),
    ('ASUS GeForce® GTX 1070 STRIX-GTX1070-8G-GAMING (90YV09N2-M0NA00)', 649, ''),
    ('MSI GeForce® GTX 1080 Armor 8G OC, 8GB GDDR5X (V336-004R)', 799, '');
    
INSERT INTO COUNTRIES (NAME)
	VALUES ('Österreich'),
    ('Deutschland');
    
INSERT INTO CITIES (NAME, COUNTRY_ID)
	VALUES ('Zwettl', 1),
    ('Gmünd', 1),
    ('St. Pölten', 1),
    ('Berlin', 2),
    ('Hamburg', 2);
    
INSERT INTO BANK_INFORMATIONS (NAME, IBAN, BIC)
	VALUES ('Volksbank', 'AT54-4715-0448-3921-0000', 'VBOEATWWNOM'),
    ('Bank Austria', 'AT69-1200-0100-1740-6793', 'BKAUATWW');
    
INSERT INTO COMPANIES (OWNER_NAME, NAME, STREET_NAME, STREET_NUMBER, PHONE, MAIL, INTERNET, LOGO_PATH, VAT, SLOGAN, ADVERTISMENT, CITY_ID, BANK_INFORMATION_ID)
	VALUES ('Nico Windtner', 'Windtner GmbH', 'Teichgasse', '173', '+43660 6000958', 'n.windtner@htlkrems.at', 'www.windtner.at', 'https://cgi.cryptoreport.com/images/coins/snt.svg', '84812839311', 'We are the best!', 'Lorem Ipsum', 1, 1),
    ('Niklas Moedlagl', 'Niki Moedi GmbH', 'Feldweg', '9', '+43680 3318653', 'n.moedlagl@htlkrems.at', 'www.niki-moedi.at', 'http://lofrev.net/wp-content/photos/2016/06/pepsi-logo-vector-1007x806.png', '8451928891', 'I am not creative!', 'Lorem Ipsum', 4, 2);
    
INSERT INTO ORDERS (DATE, DISCOUNT, CUSTOMER_ID, CONTRACTOR_ID)
	VALUES (CURRENT_DATE, 0, 4, 3),
    ('2018-02-08', 10, 3, 4);
    
INSERT INTO ORDERS_ARTICLES (ORDER_ID, ARTICLE_ID, AMOUNT)
	VALUES (1, 1, 2),
    (1, 2, 1),
    (1, 5, 3),
    (2, 1, 1),
    (2, 4, 2);