﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI
{
    public partial class Window : Form
    {

        Pen pen;

        public Window()
        {
            InitializeComponent();

            pen = new Pen(Brushes.Red);
        }

        private void Window_Paint(object sender, PaintEventArgs e)
        {
            #region Line
            pen.Width = 1F;
            e.Graphics.DrawLine(
                pen,
                new Point(20, 20),
                new Point(520, 20)
            );
            pen.Width = 2F;
            e.Graphics.DrawLine(
                pen,
                new Point(20, 40),
                new Point(520, 40)
            );
            pen.Width = 4F;
            e.Graphics.DrawLine(
                pen,
                new Point(20, 60),
                new Point(520, 60)
            );
            #endregion

            #region Lines
            pen.Width = 2F;
            e.Graphics.DrawLines(
                pen,
                new Point[]  {
                    new Point(20, 80),
                    new Point(50, 120),
                    new Point(80, 80),
                    new Point(110, 120),
                    new Point(140, 80),
                    new Point(170, 120),
                    new Point(200, 80),
                    new Point(230, 120),
                    new Point(260, 80)
                }
            );
            #endregion
        }
    }
}
