﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassTest
{
    public class Vector
    {

        int[] values;

        public Vector()
        {
            values = new int[10];
        }

        public static Vector operator +(Vector a, Vector b)
        {
            Vector v = new Vector();

            for (int i = 0; i < a.values.Length; i++)
            {
                v.values[i] = a.values[i] + b.values[i];
            }

            return v;
        }

        public static int operator *(Vector a, Vector b)
        {
            int sum = 0;

            for (int i = 0; i < a.values.Length; i++)
            {
                sum += a.values[i] * b.values[i];
            }

            return sum;
        }

    }
}
