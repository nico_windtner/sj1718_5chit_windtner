﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            List<StructTest.Vector> structList = new List<StructTest.Vector>();
            List<ClassTest.Vector> classList = new List<ClassTest.Vector>();

            for (int i = 0; i < 1000000; i++)
            {
                structList.Add(new StructTest.Vector(1));
                classList.Add(new ClassTest.Vector());
            }

            Console.WriteLine("Calculating...");

            StructTest.Vector v = new StructTest.Vector(1);
            ClassTest.Vector cv = new ClassTest.Vector(); ;
            int sum = 0;

            Stopwatch watchStructAdd = new Stopwatch();
            watchStructAdd.Start();
            for (int i = 0; i < structList.Count; i++)
                v += structList[i];
            watchStructAdd.Stop();

            Console.WriteLine("Struct Add Time (100000): " + watchStructAdd.ElapsedMilliseconds);

            Stopwatch watchStructMultiply = new Stopwatch();
            watchStructMultiply.Start();
            for (int i = 0; i < structList.Count - 1; i++)
                sum += structList[i] * structList[i + 1];
            watchStructMultiply.Stop();

            Console.WriteLine("Struct Multiply Time (100000): " + watchStructMultiply.ElapsedMilliseconds + "\n");

            Stopwatch watchClassAdd = new Stopwatch();
            watchClassAdd.Start();
            for (int i = 0; i < classList.Count; i++)
                cv += classList[i];
            watchClassAdd.Stop();

            Console.WriteLine("Class Add Time (100000): " + watchClassAdd.ElapsedMilliseconds);

            Stopwatch watchClassMultiply = new Stopwatch();
            watchClassMultiply.Start();
            for (int i = 0; i < classList.Count - 1; i++)
                sum += classList[i] * classList[i + 1];
            watchClassMultiply.Stop();

            Console.WriteLine("Class Multiply Time (100000): " + watchClassMultiply.ElapsedMilliseconds);
            Console.ReadKey();
        }
    }
}
