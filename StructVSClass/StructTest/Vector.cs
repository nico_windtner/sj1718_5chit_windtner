﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructTest
{
    public struct Vector
    {

        int[] values;

        public Vector(int amount)
        {
            values = new int[10];
        }

        public static Vector operator +(Vector c1, Vector c2)
        {
            if (c1.values.Length != c2.values.Length || c1.values.Length != 10)
                throw new Exception("Vectors can not be added!");

            Vector v;
            v.values = new int[10];

            for (int i = 0; i < c1.values.Length; i++)
                v.values[i] = c1.values[i] + c2.values[i];

            return v;
        }

        public static int operator *(Vector c1, Vector c2)
        {
            if (c1.values.Length != c2.values.Length)
                throw new Exception("Vectors can not be added!");

            int sum = 0;

            for (int i = 0; i < c1.values.Length; i++)
                sum += c1.values[i] * c2.values[i];

            return sum;
        }

    }
}
