﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp3Exercices
{
    static class ExtendInteger
    {

        public static int SumDigits(this int number)
        {
            int sum = 0;

            number = (number < 0) ? (number * (-1)) : number;

            while (number > 0)
            {
                sum += number % 10;
                number /= 10;
            }

            return sum;
        }

    }
}
