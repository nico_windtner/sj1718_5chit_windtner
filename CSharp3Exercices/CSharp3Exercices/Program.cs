﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp3Exercices
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("String: ");
            Console.WriteLine(Console.ReadLine().Swap("ß", "ss"));

            string input;
            int number;
            Console.Write("Number: ");
            input = Console.ReadLine();
            Int32.TryParse(input, out number);
            Console.WriteLine(number.SumDigits());

            List<Student> students = new List<Student>() {
                new Student() { Name="Nico", Age=18 },
                new Student() { Name="Fred", Age=42 },
                new Student() { Name="Lisa", Age=23 },
                new Student() { Name="Franz", Age=61 },
                new Student() { Name="Michael", Age=15 },
                new Student() { Name="Kevin", Age=18 }
            };
            /*
            IEnumerable<Student> sortedStudents =
                from student in students
                orderby student.Age ascending, student.Name ascending
                select student;
            */
            /*
            IEnumerable<Student> sortedStudents =
                students.OrderBy(student => student.Age).ThenBy(student => student.Name);
            */
            IEnumerable<Student> sortedStudents =
                from student in students
                where student.Age > 22
                orderby student.Name ascending
                select student;
            foreach (Student item in sortedStudents)
                Console.WriteLine(item.ToString());

            Console.ReadKey();

        }
    }
}
