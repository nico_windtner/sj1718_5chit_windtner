﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp3Exercices
{
    static class ExtendString
    {
        /*
        public static string Swap(this string data)
        {
            return data.Replace("ß", "ss");
        }
        */
        public static string Swap(this string data, string oldString, string newString)
        {
            return data.Replace(oldString, newString);
        }

    }
}
