﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using XmlHandler;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public TcpClient Client { get; set; }
        public Stream Stream { get; set; }
        public StreamReader Reader { get; set; }
        public StreamWriter Writer { get; set; }
        public String SourceUri { get; set; }


        public MainWindow()
        {
            InitializeComponent();
            this.ConnectToServer();
            this.Stream = this.Client.GetStream();
            this.Reader = new StreamReader(this.Stream);
            this.Writer = new StreamWriter(this.Stream);
            this.Writer.AutoFlush = true;
            Receive();
        }


        private void ConnectToServer()
        {
            int count = 10;

            while (true)
            {
                if (count == 0)
                    this.Close();

                try
                {
                    this.Client = new TcpClient(Dns.GetHostName(), 8080);
                    break;
                }
                catch (Exception)
                {
                    count--;
                    if (count == 9)
                        MessageBox.Show("Trying to connect to the server...");
                    Thread.Sleep(1000);
                }
            }
        }
        public async Task Receive()
        {
            string xml;

            xml = await Task.Run(() => Reader.ReadLineAsync());
            Manage(xml);

        }
        public void Manage(string xml)
        {
            ServerInformation serverInformation = XmlConverter.GetServerInformation(xml);
            int life = serverInformation.Lifes;
            lblLifes.Content = lblLifes.Content.ToString().Substring(0, 6) + " " + life;
            Drawer.UpdateHangman(img, life);
            string word = "";
            foreach (Char item in serverInformation.Word)
                word += item + " ";
            lblWord.Content = word;
            if (serverInformation.ResponseType == ResponseType.Wrong)
            {
                lblLifes.Foreground = Brushes.IndianRed;
            }
            else if (serverInformation.ResponseType == ResponseType.Right)
            {
                lblLifes.Foreground = Brushes.LightGreen;
            }
            else if (serverInformation.ResponseType == ResponseType.Won)
            {
                lblLifes.Foreground = Brushes.LightGreen;
                MessageBox.Show("You won!");
                rect.Visibility = Visibility.Visible;
                btnPlayAgain.Visibility = Visibility.Visible;
            }
            else if (serverInformation.ResponseType == ResponseType.Lost)
            {
                lblLifes.Foreground = Brushes.IndianRed;
                MessageBox.Show("You lost!");
                rect.Visibility = Visibility.Visible;
                btnPlayAgain.Visibility = Visibility.Visible;
            }
        }
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            ClientInformation client = new ClientInformation();
            client.Letter = btn.Content.ToString().ToLower()[0];
            try
            {
                Writer.WriteLine(XmlConverter.CreateXml(client));
            }
            catch (Exception)
            {
                MessageBox.Show("Server offline! This window will be closed now...");
                this.Close();
                return;
            }
            Receive();

            btn.IsEnabled = false;
        }

        private void btnPlayAgain_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            this.Close();
        }
    }
}
