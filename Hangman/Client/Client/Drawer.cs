﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Client
{
    class Drawer
    {
        public static void UpdateHangman(Image img, Int32 life)
        {
            switch (life)
            {
                case 0:
                    img.Source = new BitmapImage(new Uri(@"img/step_11.png", UriKind.RelativeOrAbsolute));
                    break;
                case 1:
                    img.Source = new BitmapImage(new Uri(@"img/step_10.png", UriKind.RelativeOrAbsolute));
                    break;
                case 2:
                    img.Source = new BitmapImage(new Uri(@"img/step_9.png", UriKind.RelativeOrAbsolute));
                    break;
                case 3:
                    img.Source = new BitmapImage(new Uri(@"img/step_8.png", UriKind.RelativeOrAbsolute));
                    break;
                case 4:
                    img.Source = new BitmapImage(new Uri(@"img/step_7.png", UriKind.RelativeOrAbsolute));
                    break;
                case 5:
                    img.Source = new BitmapImage(new Uri(@"img/step_6.png", UriKind.RelativeOrAbsolute));
                    break;
                case 6:
                    img.Source = new BitmapImage(new Uri(@"img/step_5.png", UriKind.RelativeOrAbsolute));
                    break;
                case 7:
                    img.Source = new BitmapImage(new Uri(@"img/step_4.png", UriKind.RelativeOrAbsolute));
                    break;
                case 8:
                    img.Source = new BitmapImage(new Uri(@"img/step_3.png", UriKind.RelativeOrAbsolute));
                    break;
                case 9:
                    img.Source = new BitmapImage(new Uri(@"img/step_2.png", UriKind.RelativeOrAbsolute));
                    break;
                case 10:
                    img.Source = new BitmapImage(new Uri(@"img/step_1.png", UriKind.RelativeOrAbsolute));
                    break;
                case 11:
                    img.Source = new BitmapImage(new Uri(@"img/step_0.png", UriKind.RelativeOrAbsolute));
                    break;
                default:
                    break;
            }
        }
    }
}
