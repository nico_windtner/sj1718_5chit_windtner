﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Server;

namespace HangmanServer
{
    class Program
    {
        public static Server.Server server;


        static void Main(string[] args)
        {
            server = new Server.Server();

            WaitOnClose();
        }
        static public void WaitOnClose()
        {
            while (true)
                if (Console.ReadLine() == "exit")
                {
                    server.Stop();
                    break;
                }
        }
    }
}
