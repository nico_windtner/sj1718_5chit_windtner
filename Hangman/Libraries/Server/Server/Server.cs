﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using XmlHandler;

namespace Server
{
    public class Server
    {
        public TcpListener Listener { get; set; }
        public List<ClientData> Clients { get; set; }
        public Thread listening;


        public Server()
        {
            this.Listener = new TcpListener(8080);
            this.Clients = new List<ClientData>();
            this.listening = new Thread(Start);
            this.listening.Start();
        }

        // Starts a listener and waits for incoming clients,
        // if a client connections it will create a new ClientData instance
        // where the information of a client gets saved
        public void Start()
        {
            try
            {
                Listener.Start();
                while (true)
                    Clients.Add(new ClientData(this, Listener.AcceptSocket()));
            }
            catch (Exception) { }
        }
        // Waits for the user to send a letter and responds
        // with all useful information the user needs to know
        public void Receive(object o)
        {
            ClientData client = (ClientData)o;
            Stream stream = new NetworkStream(client.Socket);
            StreamReader reader;
            StreamWriter writer;

            try
            {
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                SendStartUpWord(writer, client);

                while (true)
                {
                    string xml = reader.ReadLine();
                    ClientInformation clientInformation = XmlConverter.GetClientInformation(xml);
                    ResponseType responseType = client.TakeGuess(clientInformation.Letter);
                    ServerInformation serverInformation = new ServerInformation();
                    serverInformation.ResponseType = responseType;
                    serverInformation.Lifes = client.Lifes;
                    serverInformation.Word = client.WordManager.GuessedWord;
                    writer.WriteLine(XmlConverter.CreateXml(serverInformation));
                    if (responseType == ResponseType.Lost)
                        break;
                }

                reader.Close();
                writer.Close();
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                stream.Close();
                client.Socket.Close();
            }
        }
        // Server stops and disconnects all client the registered on the server
        public void Stop()
        {
            foreach (ClientData item in Clients)
            {
                item.Disconnect();
                Clients.Remove(item);
            }

            this.Listener.Stop();
        }
        // When a user first connects, all the information he needs to know
        // for start guessing a word will be sent
        private void SendStartUpWord(StreamWriter writer, ClientData client)
        {
            ServerInformation serverInformation = new ServerInformation();
            serverInformation.ResponseType = ResponseType.Default;
            serverInformation.Lifes = client.Lifes;
            serverInformation.Word = client.WordManager.GuessedWord;
            writer.WriteLine(XmlConverter.CreateXml(serverInformation));
        }
    }
}
