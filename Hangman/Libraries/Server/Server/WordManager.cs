﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using XmlHandler;

namespace Server
{
    public class WordManager
    {
        public static Random _Random = new Random();
        public static String _DefaultWord = "apple";

        public String Word { get; set; }
        public String GuessedWord { get; set; }
        public List<Char> GuessedLetters { get; set; }


        public WordManager()
        {
            this.Word = GetRandomWord("words.txt");
            this.GuessedLetters = new List<char>();
            this.GuessedWord = "";
            foreach (Char item in Word)
                this.GuessedWord += '‗';
        }


        // Read a random word from a file and returns it.
        // In case the file doesn't exist it will return a default word.
        private string GetRandomWord(String filepath)
        {
            List<String> words = new List<String>();
            StreamReader reader;

            if (!File.Exists(filepath))
                return _DefaultWord;
            reader = new StreamReader(filepath);
            while (reader.Peek() != -1)
                words.Add(reader.ReadLine());
            reader.Close();
            if (words.Count == 0)
                return _DefaultWord;
            return words[_Random.Next(0, words.Count())].ToLower();
        }
        // Check if the word contains the letter
        public ResponseType TakeGuess(Char letter)
        {
            if (GuessedLetters.Contains(letter.ToString().ToLower()[0]))
                return ResponseType.Wrong;
            GuessedLetters.Add(letter.ToString().ToLower()[0]);
            if (!Word.ToLower().Contains(letter.ToString().ToLower()[0]))
                return ResponseType.Wrong;
            for (Int32 i = 0; (i = Word.ToLower().IndexOf(letter.ToString().ToLower()[0], i)) != -1; i++)
            {
                if (GuessedWord.Length == i + 1)
                    GuessedWord = GuessedWord.Substring(0, i) + letter.ToString().ToLower()[0];
                else
                    GuessedWord = GuessedWord.Substring(0, i) + letter.ToString().ToLower()[0] + GuessedWord.Substring(i + 1);
            }
            if (GuessedWord == Word)
                return ResponseType.Won;
            return ResponseType.Right;
        }
    }
}
