﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using XmlHandler;

namespace Server
{
    public class ClientData
    {
        public Thread Thread { get; set; }
        public Socket Socket { get; set; }
        public WordManager WordManager;
        public Int32 Lifes { get; set; }


        public ClientData(Server server, Socket socket)
        {
            this.Socket = socket;
            this.WordManager = new WordManager();
            this.Lifes = 11;
            this.Thread = new Thread(server.Receive);
            this.Thread.Start(this);
        }


        // The guessed letter gets passed as an char.
        // The Method passes the letter to the WordManager
        // and then handles the ResponseType in order to modify
        // and change the attributes/information of an connected client
        public ResponseType TakeGuess(Char letter)
        {
            switch (WordManager.TakeGuess(letter))
            {
                case ResponseType.Wrong:
                    Lifes -= 1;
                    if (Lifes == 0)
                        return ResponseType.Lost;
                    return ResponseType.Wrong;
                case ResponseType.Right:
                    return ResponseType.Right;
                case ResponseType.Won:
                    return ResponseType.Won;
            }
            return ResponseType.Default;
        }
        // Closes the receive thread and disconnects the client
        public void Disconnect()
        {
            Thread.Abort();
            throw new Exception();
        }
    }
}
