﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlHandler
{
    public class ServerInformation
    {
        public ResponseType ResponseType { get; set; }
        public Int32 Lifes { get; set; }
        public String Word { get; set; }


        public ServerInformation()
        {

        }
        public ServerInformation(ResponseType responseType, Int32 lifes, String word)
        {
            this.ResponseType = responseType;
            this.Lifes = lifes;
            this.Word = word;
        }
    }
}
