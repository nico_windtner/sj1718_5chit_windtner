﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XmlHandler
{
    public class XmlConverter
    {
        // Creates an XML-String out of an instance of ServerInformation
        public static string CreateXml(ServerInformation server)
        {
            XmlDocument document = new XmlDocument();
            XmlElement information = document.CreateElement("Data");
            XmlElement responseType = document.CreateElement("ResponseType");
            XmlElement lifeCount = document.CreateElement("LifeCount");
            XmlElement word = document.CreateElement("Word");

            document.AppendChild(information);
            information.AppendChild(responseType);
            information.AppendChild(lifeCount);
            information.AppendChild(word);
            responseType.InnerText = server.ResponseType.ToString();
            lifeCount.InnerText = server.Lifes.ToString();
            word.InnerText = server.Word;
            return document.OuterXml;
        }
        // Creates and instance of ClientInformation and reads and sets its
        // attributes out of the XML-String passed as a parameter
        public static ClientInformation GetClientInformation(String xml)
        {
            ClientInformation clientInformation = new ClientInformation();
            XmlDocument document = new XmlDocument();
            XmlNodeList information;

            document.LoadXml(xml);
            information = document.SelectNodes("/Data");
            foreach (XmlNode node in information)
                clientInformation.Letter = node["Letter"].InnerText[0];
            return clientInformation;
        }
        // Creates an XML-String out of an instance of ClientInformation
        public static string CreateXml(ClientInformation client)
        {
            XmlDocument document = new XmlDocument();
            XmlElement information = document.CreateElement("Data");
            XmlElement letter = document.CreateElement("Letter");

            document.AppendChild(information);
            information.AppendChild(letter);
            letter.InnerText = client.Letter.ToString();
            return document.OuterXml;
        }
        // Creates and instance of ServerInformation and reads and sets its
        // attributes out of the XML-String passed as a parameter
        public static ServerInformation GetServerInformation(String xml)
        {
            ServerInformation serverInformation = new ServerInformation();
            XmlDocument document = new XmlDocument();
            XmlNodeList information;

            document.LoadXml(xml);
            information = document.SelectNodes("/Data");
            foreach (XmlNode node in information)
            {
                switch (node["ResponseType"].InnerText)
                {
                    case "Default":
                        serverInformation.ResponseType = ResponseType.Default;
                        break;
                    case "Right":
                        serverInformation.ResponseType = ResponseType.Right;
                        break;
                    case "Wrong":
                        serverInformation.ResponseType = ResponseType.Wrong;
                        break;
                    case "Won":
                        serverInformation.ResponseType = ResponseType.Won;
                        break;
                    case "Lost":
                        serverInformation.ResponseType = ResponseType.Lost;
                        break;
                }
                serverInformation.Lifes = Convert.ToInt32(node["LifeCount"].InnerText);
                serverInformation.Word = node["Word"].InnerText;
            }
            return serverInformation;
        }
    }
}
