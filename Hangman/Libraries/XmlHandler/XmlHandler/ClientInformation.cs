﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlHandler
{
    public class ClientInformation
    {
        public Char Letter { get; set; }


        public ClientInformation()
        {

        }
        public ClientInformation(Char letter)
        {
            this.Letter = letter;
        }
    }
}
