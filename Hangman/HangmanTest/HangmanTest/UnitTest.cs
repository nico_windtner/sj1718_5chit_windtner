﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Server;
using XmlHandler;

namespace HangmanTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void WordManager_Check_Right_Method_Test()
        {
            WordManager manager = new WordManager();
            manager.Word = "apple";

            ResponseType type = manager.TakeGuess('a');

            Assert.AreEqual(ResponseType.Right, type);
        }

        [TestMethod]
        public void WordManager_Check_Wrong_Method_Test()
        {
            WordManager manager = new WordManager();
            manager.Word = "apple";

            ResponseType type = manager.TakeGuess('z');

            Assert.AreEqual(ResponseType.Wrong, type);
        }

        [TestMethod]
        public void WordManager_Check_Won_Method_Test()
        {
            WordManager manager = new WordManager();
            manager.Word = "apple";
            manager.GuessedWord = "_pple";

            ResponseType type = manager.TakeGuess('a');

            Assert.AreEqual(ResponseType.Won, type);
        }
    }
}
