﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace Server
{
    class Server
    {

        private TcpListener listener;

        private Dictionary<int, Game> games;

        public Thread listening;

        public Server()
        {
            this.listener = new TcpListener(3333);
            this.games = new Dictionary<int, Game>();
            this.listening = new Thread(Listen);
            this.listening.Start();
        }

        private void Start()
        {
            int gameCount = 0;

            try
            {
                listener.Start();
                while (true)
                {
                    gameCount++;
                    games.Add(gameCount, new Game(this, listener.AcceptSocket()));
                }
            }
            catch (Exception) { }
        }

        public void Receive(object o)
        {
            Game game = (Game)o;
            NetworkStream stream = new NetworkStream(game.Socket);
            StreamReader reader;
            StreamWriter writer;

            try
            {
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                while (true)
                {
                    string xml = reader.ReadLine();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
