﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    class Game
    {

        public Thread Thread { get; private set; }

        public Socket Socket { get; private set; }

        private List<Question> questions;

        public Game(Server server, Socket socket, List<Question> questions)
        {
            this.Socket = socket;
            this.questions = questions;

            this.Thread = new Thread(server.Receive);
            this.Thread.Start();
        }

    }
}
