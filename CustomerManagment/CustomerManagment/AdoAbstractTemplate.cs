﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace CustomerManagment
{
    abstract class AdoAbstractTemplate<IBo> : AbstractDal<IBo>
    {

        SqlConnection objConnection;
        protected SqlCommand objCommand;

        void Open()
        {

            objConnection = new SqlConnection(@"Data Source=localhost\SQL2008;Initial Catalog=DbCustomer;Integrated Security=True");
            objConnection.Open();
            objCommand = new SqlCommand();
            objCommand.Connection = objConnection;
        }

        void Close()
        {
            objConnection.Close();

        }

        protected abstract void ExecuteQuery(IBo obj);

        protected abstract List<IBo> ExecuteNonQuery();
        // Design pattern :- Template pattern

        public void Execute(IBo obj)
        {
            Open();
            ExecuteQuery(obj);
            Close();
        }

        public List<IBo> ExecuteRead()
        {
            List<IBo> obj = new List<IBo>();
            Open();
            obj = ExecuteNonQuery();
            Close();
            return obj;
        }

        public override void Save()
        {
            foreach (IBo i in Collection)
            {
                Execute(i);
            }
        }

    }
}
