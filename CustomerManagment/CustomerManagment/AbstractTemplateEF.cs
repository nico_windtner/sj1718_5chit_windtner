﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.E;

namespace CustomerManagment
{
    class AbstractTemplateEF<Anytype> : IDataLayer<Anytype> where Anytype : class
    {

        public void Add(Anytype obj)
        {

            Set<Anytype>().Add(obj);
        }

        public void Delete(Anytype obj)
        {
            Set<Anytype>().Remove(obj);
        }

        public void Save()
        {
            SaveChanges();

        }
        public virtual List<Anytype> Get()
        {
            return Set<Anytype>().AsQueryable<Anytype>().ToList<Anytype>();
        }

    }
}
