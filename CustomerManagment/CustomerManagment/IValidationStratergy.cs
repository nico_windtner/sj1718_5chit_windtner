﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    interface IValidationStratergy<AnyType>
    {

        void Validate(AnyType obj);

    }
}
