﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    class Lead : CustomerBase
    {

        public Lead(IValidationStratergy<ICustomer> obj) : base(obj)
        {

        }

        public override string Type
        {
            get
            {
                return "Lead";
            }
            set
            {
                _type = value;
            }
        }

    }
}
