﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    interface ICustomer : IBo
    {

        IValidationStratergy<ICustomer> ValidationType { get; }

        string CustomerName { get; set; }

        string PhoneNumber { get; set; }

        decimal BillAmount { get; set; }

        DateTime BillDate { get; set; }

        string Address { get; set; }

        string Type { get; set; }

        int Id { get; set; }

        ICustomer Clone();

    }
}
