﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    abstract class BoBase : IBo
    {

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public virtual void Validate()
        {
            throw new NotImplementedException();
        }

    }
}
