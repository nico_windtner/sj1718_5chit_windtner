﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace CustomerManagment
{
    class CustomerDal : AdoAbstractTemplate<ICustomer>
    {

        public override List<ICustomer> Get()
        {
            return ExecuteRead();
        }
        protected override List<ICustomer> ExecuteNonQuery()
        {
            List<ICustomer> o = new List<ICustomer>();
            objCommand.CommandText = "select * from tblcust";
            SqlDataReader rd = objCommand.ExecuteReader();
            while (rd.Read())
            {
                ICustomer i = Factory<ICustomer>.Create(rd["CustomerType"].ToString());
                i.Type = rd["CustomerType"].ToString();
                i.CustomerName = rd["CustomerName"].ToString();
                i.PhoneNumber = rd["PhoneNumber"].ToString();
                i.BillDate = Convert.ToDateTime(rd["BillDate"].ToString());
                i.Address = rd["Address"].ToString();
                i.Id = Convert.ToInt16(rd["Id"].ToString());
                i.BillAmount = Convert.ToDecimal(rd["BillAmount"].ToString());
                o.Add(i);
            }
            return o;
        }
        protected override void ExecuteQuery(ICustomer obj)
        {
            objCommand.CommandText = "insert into tblcust values('"
                                    + obj.CustomerName +
                                    "','" + obj.PhoneNumber
                                    + "'," + obj.BillAmount
                                    + ",'" + obj.BillDate + "','" +
                                    obj.Address + "','"
                                    + obj.Type + "')";
            objCommand.ExecuteNonQuery();
        }

    }
}
