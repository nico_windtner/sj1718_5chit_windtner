﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    class CustomerEfDal : AbstractTemplateEF<CustomerBase>
    {

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerBase>().ToTable("tblCust");
            modelBuilder.Entity<CustomerBase>()
                .Map<Customer>(m => m.Requires("CustomerType").HasValue("Customer"))
                .Map<Lead>(m => m.Requires("CustomerType").HasValue("Lead"));
            modelBuilder.Entity<CustomerBase>().Ignore(t => t.Type);
            // base.OnModelCreating(modelBuilder);
        }

    }
}
