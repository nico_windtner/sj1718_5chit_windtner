﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    interface IBo
    {

        int Id { get; set; }

        void Validate();

    }
}
