﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerManagment
{
    class Customer : CustomerBase
    {

        public Customer(IValidationStratergy<ICustomer> obj) : base(obj)
        {

        }

        public override string Type
        {
            get
            {
                return "Customer";
            }
            set
            {
                _type = value;
            }
        }

    }
}
