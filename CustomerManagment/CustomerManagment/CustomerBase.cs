﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CustomerManagment
{
    abstract class CustomerBase : BoBase, ICustomer
    {
        private IValidationStratergy<ICustomer> _ValidationType = null;

        public IValidationStratergy<ICustomer> ValidationType
        {
            get
            {
                return _ValidationType;
            }

        }

        public string CustomerName { get; set; }

        public string PhoneNumber { get; set; }

        public decimal BillAmount { get; set; }

        public DateTime BillDate { get; set; }

        public string Address { get; set; }

        public override void Validate()
        {
            ValidationType.Validate(this);
        }

        public void init()
        {
            CustomerName = "";
            PhoneNumber = "";
            BillAmount = 0;
            BillDate = DateTime.Now;
            Address = "";
        }

        public CustomerBase(IValidationStratergy<ICustomer> _Validate)
        {
            _ValidationType = _Validate;
            init();
        }

        [Key]
        public int Id { get; set; }
        protected string _type = "B";
        public virtual string Type
        {
            get
            {
                return "B";
            }
            set
            {
                _type = value;
            }
        }

        IValidationStratergy<ICustomer> ICustomer.ValidationType => throw new NotImplementedException();

        public ICustomer Clone()
        {
            return (CustomerBase)this.MemberwiseClone();
            // Design pattern :- Prototype pattern
        }

    }
}
