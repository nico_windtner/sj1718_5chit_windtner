﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConvolutionOperation;

namespace OperationsTesting
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestPlusOperator()
        {
            Matrix input = new Matrix(
                new Double[8, 10]
                {
                    { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                    { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                    { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
                    { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 },
                    { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 },
                    { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
                    { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 },
                    { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }
                }
            );
            Matrix kernel = new Matrix(
                new Double[3, 3]
                {
                    { 3, 6, 9 },
                    { 6, 9, 12 },
                    { 9, 12, 15 }
                }
            );
            Matrix output;

            output = input * kernel;
            Assert.AreEqual("| 279 360 441 522 603 684 765 846 |\n" +
                "| 360 441 522 603 684 765 846 927 |\n" +
                "| 441 522 603 684 765 846 927 1008 |\n" +
                "| 522 603 684 765 846 927 1008 1089 |\n" +
                "| 603 684 765 846 927 1008 1089 1170 |\n" +
                "| 684 765 846 927 1008 1089 1170 1251 |\n", output);
        }
    }
}
