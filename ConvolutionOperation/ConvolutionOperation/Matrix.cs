﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionOperation
{
    public class Matrix
    {

        private Double[,] values;

        public Matrix(Double[,] values)
        {
            this.values = values;
        }

        public static Matrix operator *(Matrix input, Matrix kernel)
        {
            Double kernelLength = kernel.values.GetLength(0);

            if (kernelLength != kernel.values.GetLength(1))
                throw new Exception("Kernel matrix has to me symmetric!");

            Double inputX = input.values.GetLength(0);
            Double inputY = input.values.GetLength(1);

            if (kernelLength >= inputX
                || kernelLength >= inputY)
                throw new Exception("Kernel matrix is bigger than the input matrix!");

            Double kernelDiff = kernelLength - 1;
            Int32 outputX = Convert.ToInt32(inputX - kernelDiff);
            Int32 outputY = Convert.ToInt32(inputY - kernelDiff);

            Matrix output = new Matrix(new Double[outputX, outputY]);

            Double sum;
            for (int i = 0; i < outputY; i++)
                for (int j = 0; j < outputX; j++)
                {
                    sum = 0;

                    for (int y = 0; y < kernelLength; y++)
                        for (int x = 0; x < kernelLength; x++)
                            sum += input.values[j + y, i + x] * kernel.values[y, x];

                    output.values[j, i] = sum;
                }

            return output;
        }

        public override string ToString()
        {
            String text = "";

            for (int i = 0; i < values.GetLength(0); i++)
            {
                text += "|";
                for (int j = 0; j < values.GetLength(1); j++)
                    text += " " + values[i, j];
                text += " |\n";
            }

            return text;
        }

    }
}
