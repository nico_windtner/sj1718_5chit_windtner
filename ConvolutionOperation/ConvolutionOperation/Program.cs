﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionOperation
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix input = new Matrix(
                new Double[8, 10]
                {
                    { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                    { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                    { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
                    { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 },
                    { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 },
                    { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
                    { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 },
                    { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }
                }
            );
            Matrix kernel = new Matrix(
                new Double[3, 3]
                {
                    { 3, 6, 9 },
                    { 6, 9, 12 },
                    { 9, 12, 15 }
                }
            );
            Matrix output;

            output = input * kernel;
            Console.Write(output);

            Console.ReadKey();
        }
    }
}
