﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class BasketManager
    {

        private Rail selectedRail;

        public BasketManager(Rail rail)
        {
            this.selectedRail = rail;
        }

        public void ChangeRail(Rail rail)
        {
            selectedRail = rail;
        }

        public void MoveLeft(List<Rail> rails)
        {
            if (rails[0] == selectedRail)
                selectedRail = rails[rails.Count - 1];
            else
                for (int i = 1; i < rails.Count; i++)
                    if (selectedRail == rails[i])
                        selectedRail = rails[i - 1];
        }

        public void MoveRight(List<Rail> rails)
        {
            if (rails[rails.Count - 1] == selectedRail)
                selectedRail = rails[0];
            else
                for (int i = rails.Count - 1; i >= 0; i--)
                    if (selectedRail == rails[i])
                        selectedRail = rails[i + 1];
        }

        public bool CompareRails(Rail compareToRail)
        {
            return selectedRail == compareToRail;
        }

        public Rectangle GetInvalidationRectangle()
        {
            return new Rectangle(
                selectedRail.Position.X - 15,
                selectedRail.Position.Y + selectedRail.Length - 1,
                30,
                20
            );
        }

        public Rectangle Draw(Graphics g, Brush brush)
        {
            Rectangle rectangle = GetInvalidationRectangle();

            g.FillRectangle(brush, rectangle);

            return rectangle;
        }

    }
}
