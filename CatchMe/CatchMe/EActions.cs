﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchMe
{
    enum EActions
    {

        DRAW_RAILS,
        ERASE_BALL,
        DRAW_BALL,
        ERASE_BASKET,
        DRAW_BASKET,
        DRAW_BORDER,
        GAME_OVER

    }
}
