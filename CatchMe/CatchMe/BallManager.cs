﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class BallManager
    {

        public Rail Rail { get; private set; }

        public Ball Ball;

        public void CreateNewBall(Rail newRail)
        {
            Rail = newRail;

            Ball = new Ball(
                Rail.Position.Y
            );
        }

        public bool MoveBall()
        {
            return Ball.MoveDown(Rail.Position.Y + Rail.Length);
        }

        public void DrawBall(Graphics g)
        {
            Ball.Draw(g, Rail.Position.X);
        }

        public void EraseBall(Graphics g)
        {
            Ball.Erase(g, Rail.Position.X);
        }

    }
}
