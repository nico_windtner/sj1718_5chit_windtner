﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchMe
{
    class RailsFactory
    {

        public static List<Rail> CreateRails()
        {
            List<Rail> rails = new List<Rail>();
            int length = Drawer._Field.ClientSize.Width - 120;
            int maxRails = length / 50;
            int leftSpace = ((length % 50) / 2) + 60;

            for (int i = 0; i < maxRails; i++)
            {
                rails.Add(new Rail(
                    new Position(
                        leftSpace + (i * 50) + 25,
                        80
                    ),
                    Drawer._Field.ClientSize.Height - 300
                ));
            }

            return rails;
        }

    }
}
