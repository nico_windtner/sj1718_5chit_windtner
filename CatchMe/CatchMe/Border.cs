﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class Border
    {

        private Position A;

        private Position B;

        public Border(Position A, Position B)
        {
            this.A = A;
            this.B = B;
        }

        public void DrawBorder(Graphics g)
        {
            Rectangle rectangle = new Rectangle(
                A.X,
                A.Y,
                B.X - A.X,
                B.Y - A.Y
            );

            Pen pen = new Pen(Brushes.LightSteelBlue);
            pen.Width = 2;

            g.DrawRectangle(
                pen,
                rectangle
            );
        }

    }
}
