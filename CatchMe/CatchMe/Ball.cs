﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class Ball
    {

        private static int _difference = 2;

        private int posY;

        public Ball(int posY)
        {
            this.posY = posY;
        }

        public bool MoveDown(int maxY)
        {
            posY += _difference;

            return (posY <= maxY) ? true : false;
        }

        public void Erase(Graphics g, int posX)
        {
            Rectangle rectangle = GetValidationRectangle(posX);

            g.FillEllipse(
                Brushes.White,
                rectangle
            );
        }

        public Rectangle GetValidationRectangle(int posX)
        {
            return new Rectangle(
                posX - 8,
                posY - 8,
                16,
                16
            );
        }

        public void Draw(Graphics g, int posX)
        {
            Rectangle rectangle = GetValidationRectangle(posX);

            g.FillEllipse(
                Brushes.LightSeaGreen,
                rectangle
            );
        }

    }
}
