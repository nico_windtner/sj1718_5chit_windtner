﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class Game
    {

        private static Random _random = new Random();

        public int Score { get; private set; }

        private List<Rail> rails;
        
        public Border Border { get; private set; }

        public BallManager BallManager { get; private set; }

        public BasketManager BasketManager { get; private set; }

        public Game(Border border)
        {
            Score = 0;
            this.rails = RailsFactory.CreateRails();
            this.Border = border;
            Setup();
        }

        public void DrawRails(Graphics g)
        {
            foreach (Rail item in rails)
                item.Draw(g);
        }

        private void Setup()
        {
            BallManager = new BallManager();
            BallManager.CreateNewBall(ChooseRandomRail());

            BasketManager = new BasketManager(rails[0]);
        }

        private Rail ChooseRandomRail()
        {
            return rails[_random.Next(0, rails.Count)];
        }

        public void MoveBasketLeft()
        {
            BasketManager.MoveLeft(rails);
        }

        public void MoveBasketRight()
        {
            BasketManager.MoveRight(rails);
        }

        public bool? MoveBall()
        {
            if(!BallManager.MoveBall())
            {
                if (BasketManager.CompareRails(BallManager.Rail))
                {
                    Score += 1;
                    BallManager.CreateNewBall(ChooseRandomRail());
                    return true;
                }
                else
                    return false;
            }

            return null;
        }

    }
}
