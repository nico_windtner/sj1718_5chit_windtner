﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CatchMe
{
    class Rail
    {

        public Position Position { get; private set; }

        public int Length { get; private set; }

        public Rail(Position position, int length)
        {
            this.Position = position;
            this.Length = length;
        }

        public Rectangle Draw(Graphics g)
        {
            Rectangle rectangle = new Rectangle(
                Position.X - 1,
                Position.Y - 1,
                2,
                Length
            );

            g.FillRectangle(
                Brushes.LightGray,
                rectangle
            );

            return rectangle;
        }

    }
}
