﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace CatchMe
{
    public partial class CatchMe : Form
    {

        private List<EActions> actions;

        private Game game;

        bool finished;

        public CatchMe()
        {
            InitializeComponent();

            Drawer._Field = this;
            Border border = new Border(
                new Position(30, 50),
                new Position(this.ClientSize.Width - 30, this.ClientSize.Height - 80)
            );
            game = new Game(border);

            finished = false;

            actions = new List<EActions>();
            actions.Add(EActions.DRAW_BORDER);
            actions.Add(EActions.DRAW_RAILS);
            actions.Add(EActions.DRAW_BALL);
            actions.Add(EActions.DRAW_BASKET);
            this.Invalidate();

            timer.Enabled = true;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            actions.Add(EActions.ERASE_BALL);
            Rectangle rectangle = game.BallManager.Ball.GetValidationRectangle(game.BallManager.Rail.Position.X);
            rectangle.Height += 10;
            this.Invalidate(rectangle);
            bool? success = game.MoveBall();
            if (success == false)
            {
                actions.Add(EActions.GAME_OVER);
                this.Invalidate();
                finished = true;
                timer.Enabled = false;
            }
            else if (success == true)
                lblScore.Text = "Punktestand: " + game.Score;
            actions.Add(EActions.DRAW_BALL);
            rectangle = game.BallManager.Ball.GetValidationRectangle(game.BallManager.Rail.Position.X);
            rectangle.Height += 10;
            this.Invalidate(rectangle);
        }

        private void CatchMe_KeyDown(object sender, KeyEventArgs e)
        {
            if (!finished)
            {
                if (e.KeyCode == Keys.Left)
                {
                    actions.Add(EActions.ERASE_BASKET);
                    this.Invalidate(game.BasketManager.GetInvalidationRectangle());
                    game.MoveBasketLeft();
                    actions.Add(EActions.DRAW_BASKET);
                    this.Invalidate(game.BasketManager.GetInvalidationRectangle());
                }
                else if (e.KeyCode == Keys.Right)
                {
                    actions.Add(EActions.ERASE_BASKET);
                    this.Invalidate(game.BasketManager.GetInvalidationRectangle());
                    game.MoveBasketRight();
                    actions.Add(EActions.DRAW_BASKET);
                    this.Invalidate(game.BasketManager.GetInvalidationRectangle());
                }
            }
        }

        private void CatchMe_Paint(object sender, PaintEventArgs e)
        {
            foreach (EActions item in actions)
            {
                switch (item)
                {
                    case EActions.DRAW_BORDER:
                        game.Border.DrawBorder(e.Graphics);
                        break;
                    case EActions.DRAW_RAILS:
                        game.DrawRails(e.Graphics);
                        break;
                    case EActions.ERASE_BALL:
                        game.BallManager.EraseBall(e.Graphics);
                        game.BallManager.Rail.Draw(e.Graphics);
                        break;
                    case EActions.DRAW_BALL:
                        game.BallManager.DrawBall(e.Graphics);
                        break;
                    case EActions.ERASE_BASKET:
                        game.BasketManager.Draw(e.Graphics, Brushes.White);
                        break;
                    case EActions.DRAW_BASKET:
                        game.BasketManager.Draw(e.Graphics, Brushes.RosyBrown);
                        break;
                    case EActions.GAME_OVER:
                        StringFormat format = new StringFormat();
                        format.LineAlignment = StringAlignment.Center;
                        format.Alignment = StringAlignment.Center;
                        e.Graphics.DrawString(
                            "Game Over",
                            new Font("Arial", 14, FontStyle.Italic, GraphicsUnit.Point),
                            Brushes.Black,
                            new RectangleF(
                                30F,
                                (float)this.ClientSize.Height - 180F,
                                (float)this.ClientSize.Width - 60F,
                                70F
                            ),
                            format
                        );
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
