﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Answer
    {

        public string Text { get; set; }

        public bool Right { get; set; }

        public Answer() { }

        public Answer(string text, bool right)
        {
            this.Text = text;
            this.Right = right;
        }

        public override string ToString()
        {
            return Text;
        }

    }
}
