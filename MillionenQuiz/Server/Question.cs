﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Question
    {

        public string Text { get; set; }

        public int Level { get; set; }

        public List<Answer> Answers { get; private set; }

        private Answer ChoosenAnswer;

        public Question()
        {
            this.Answers = new List<Answer>();
        }

        public void AddAnswer(Answer answer)
        {
            Answers.Add(answer);
        }

        public string ChooseAnswer(int index)
        {
            if (index >= 0 && index < Answers.Count)
            {
                ChoosenAnswer = Answers[index];

                if (ChoosenAnswer.Right)
                {
                    return "<CORRECT>true</CORRECT>";
                }
            }

            return "<CORRECT>false</CORRECT>";
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("Frage: " + Text);

            foreach (Answer item in Answers)
            {
                if (item == ChoosenAnswer)
                    builder.AppendLine("    *" + item.ToString());
                else
                    builder.AppendLine("     " + item.ToString());
            }

            return builder.ToString();
        }

        public string ToXMLInformation()
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("<LEVEL>" + Level + "</LEVEL>");
            builder.AppendLine("<QUESTION>" + Text + "</QUESTION>");

            for (int i = 0; i < Answers.Count; i++)
                builder.AppendLine("<ANSWER" + (i + 1) + ">" + Answers[i].Text + "</ANSWER" + (i + 1) + ">");

            return builder.ToString();
        }

    }
}
