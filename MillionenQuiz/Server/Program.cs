﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Question> questions = QuestionFactory.GetRankedQuestions(@"../../../fragen.txt");

            foreach (Question item in questions)
            {
                item.ChooseAnswer(0);
                Console.Write(item.ToString());
            }

            Console.WriteLine();

            foreach (Question item in questions)
                Console.Write(item.ToXMLInformation());

            Console.ReadKey();
        }
    }
}
