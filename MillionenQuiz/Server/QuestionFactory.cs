﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Server
{
    class QuestionFactory
    {

        private static Random _random = new Random();

        public static List<Question> GetRankedQuestions(string filepath)
        {
            List<Question> questions = new List<Question>();

            StreamReader reader = new StreamReader(filepath);
            string content = reader.ReadToEnd();

            Question question;
            List<string> answerStrings;
            Answer answer;
            string answerContent;
            int index;

            string[] questionStrings = content.Split(new string[] { @"\n\r" }, StringSplitOptions.RemoveEmptyEntries);
            questionStrings = questionStrings.ToList().Where(line => line[0] != '#').ToArray();

            foreach (string item in questionStrings)
            {
                question = new Question();

                try
                {
                    answerStrings = item.Split('|').ToList();

                    question.Level = Convert.ToInt32(answerStrings[0]);
                    answerStrings.RemoveAt(0);

                    index = Convert.ToInt32(answerStrings[0]);
                    answerStrings.RemoveAt(0);

                    question.Text = answerStrings[0];
                    answerStrings.RemoveAt(0);

                    while (answerStrings.Count != 0)
                    {
                        answer = new Answer();

                        answer.Text = answerStrings[0];
                        answerStrings.RemoveAt(0);
                        answer.Right = false;

                        question.AddAnswer(answer);
                    }

                    questions.Add(question);
                }
                catch (Exception) { }
            }

            List<Question> rankedQuestions = new List<Question>();
            List<Question> conditionalQuestions = new List<Question>();

            for (int i = 1; i <= 3; i++)
            {
                conditionalQuestions = questions.Where(q => q.Level == i).ToList();

                if (conditionalQuestions.Count != 0)
                    rankedQuestions.Add(conditionalQuestions[_random.Next(conditionalQuestions.Count)]);
            }

            return rankedQuestions;
        }

    }
}
